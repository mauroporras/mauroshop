# MauroShop

![Screen shot](doc/screen-shot.png)


### Dependencies

- Ruby v2.2.2
- Redis
- MySQL (Including dev library)


### Setup

Have on hand your MySQL credentials and run:
````
./bin/setup
````


### Test Suite

````
bundle exec rake
````

Using Guard + tmux
````
bundle exec guard -c
````


### Services

````
foreman start
````

Foreman will take care of `web, worker, scheduler and mailcatcher`

Afterwards, open in your browser:
- Rails: [http://localhost:3000](http://localhost:3000)
- Mailcatcher: [http://localhost:1080](http://localhost:1080)


## API Examples

Create a customer:
````
curl -X POST -H "Content-Type: application/json" -d '{
"number": "12345678",
"name": "Mauro Porras P.",
"detail": "Ruby & JS dev"
}' 'http://localhost:3000/customers.json'
````

Fetch a product:
````
curl 'http://localhost:3000/products/1.json'
````


# Scalable Deployment

- Use nginx + Passenger.
- Several AWS EC2 + RDS instances.
- Elastic Load Balancing.
- Implement S3 + CloudFront CDN for the assets.


# Security and QA

- Use a CI tool such as [https://circleci.com](https://circleci.com) to avoid regressions.
- Use a code review tool such as [https://codeclimate.com](https://codeclimate.com) to keep an eye on the code quality.
- Integrate [Rubocop](https://github.com/bbatsov/rubocop) in your dev env.
- Follow the [Rails security guidelines](http://guides.rubyonrails.org/security.html).
- Enable AWS Multi-Factor Authentication.


# Proposed and implemented ER Model

It allows the numeration of orders without affecting or creating new fields:
[doc/erd.pdf](doc/erd.pdf)
