require 'resque_web'

Rails.application.routes.draw do
  root 'welcome#index'

  resources :customers do
    get :purchases
  end
  resources :branches
  resources :products
  resources :purchases
  resources :logs, only: [:index, :show]

  resources :reports, only: [:index] do
    collection do
      match :purchases, via: [:get, :post]
      post :weekly
    end
  end

  mount ResqueWeb::Engine => '/resque_web'
end
