require 'test_helper'

class BranchTest < ActiveSupport::TestCase
  def setup
    @branch = branches(:one)
  end

  test 'look nice as string' do
    assert_equal 'Medellín, Cr 23 45', @branch.to_s
  end

  test 'validates name' do
    @branch.name = nil
    assert_not @branch.valid?, 'Branch without name is valid.'
  end

  test 'validates address' do
    @branch.address = nil
    assert_not @branch.valid?, 'Branch without address is valid.'
  end
end
