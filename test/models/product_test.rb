require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  def setup
    @product = products(:one)
  end

  test 'look nice as string' do
    assert_equal "#{@product.id} - Mouse", @product.to_s
  end

  test 'validate name' do
    @product.name = nil
    assert_not @product.valid?, 'Product without name is valid'
  end

  test 'validate description' do
    @product.description = nil
    assert_not @product.valid?, 'Product without description is valid'
  end

  test 'validate price' do
    @product.price = nil
    assert_not @product.valid?, 'Product without price is valid'
  end

  test 'price is a number' do
    @product.price = 'abc'
    assert_not @product.valid?, 'Product with a non-numeric price is valid'
  end
end
