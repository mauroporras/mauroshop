class Branch < ActiveRecord::Base
  include Loggable

  has_many :purchases

  validates :name, presence: true
  validates :address, presence: true

  def to_s
    "#{name}, #{address}"
  end
end
