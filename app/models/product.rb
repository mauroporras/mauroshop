class Product < ActiveRecord::Base
  include Loggable

  has_many :purchases

  validates :name, presence: true
  validates :description, presence: true
  validates :price, presence: true, numericality: true

  scope :not_void, -> { where.not(name: '(Void)') }

  def to_s
    "#{id} - #{name}"
  end
end
