class Report
  @@from = 1.week.ago.at_midnight

  def self.weekly
    {
      max: extreme_diff,
      min: extreme_diff(:min),
      avg: avg_diff,
      count: purchases_count,
      profits: profits,
      purchases_minute: purchases_minute
    }
  end

  def self.extreme_diff(type = :max, from = @@from)
    order = 'desc'
    order = 'asc' if type == :min

    ActiveRecord::Base.connection.execute("select
      a.id as product_id,
      a.name as product_name,
      b.id as purchase_id,
      b.created_at as purchase_date,
      a.price as product_price,
      b.price as purchase_price,
      b.price - a.price as diff
      from products as a join purchases as b on a.id = b.product_id
      where b.price - a.price != 0 and b.created_at >= '#{from}'
      order by abs(diff) #{order}
      limit 1").first
  end

  def self.avg_diff(from = @@from)
    ActiveRecord::Base.connection.execute("select
      avg(abs(b.price - a.price)) as avg_diff
      from products as a join purchases as b on a.id = b.product_id
      where b.price - a.price != 0 and b.created_at >= '#{from}'").first[0]
  end

  def self.purchases_count(from = @@from)
    Purchase.where('created_at >= ?', from).count
  end

  def self.profits(from = @@from)
    Purchase.where('created_at >= ?', from).sum :price
  end

  def self.purchases_minute(from = @@from)
    minutes = (Time.current.tomorrow.at_midnight - from) / 60
    purchases_count / minutes
  end
end
