class Log < ActiveRecord::Base
  def self.create_entry(operation, model)
    create operation: operation,
           model_class: model.class,
           model_id: model.id,
           model_json: model.to_json
  end

  def model_json_pretty
    JSON.pretty_generate(JSON.parse(model_json))
  end

  def to_s
    "#{model_class}: #{model_id}"
  end
end
