module Loggable
  extend ActiveSupport::Concern

  included do
    after_create do |m|
      Log.create_entry 'CREATE', m
    end

    after_update do |m|
      Log.create_entry 'UPDATE', m
    end

    after_destroy do |m|
      Log.create_entry 'DESTROY', m
    end
  end
end
