class WelcomeController < ApplicationController
  def index
    @weekly_report_email = Setting.weekly_report_email
  end
end
