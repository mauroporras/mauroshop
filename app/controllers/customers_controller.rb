class CustomersController < ApplicationController
  before_action :set_customer, only: [:show, :edit, :update, :destroy, :purchases]

  # GET /customers
  # GET /customers.json
  def index
    @customers = Customer.order :name
  end

  # GET /customers/1
  # GET /customers/1.json
  def show
  end

  # GET /customers/new
  def new
    @customer = Customer.new
  end

  # GET /customers/1/edit
  def edit
  end

  # POST /customers
  # POST /customers.json
  def create
    @customer = Customer.new(customer_params)

    respond_to do |format|
      if @customer.save
        format.html { redirect_to @customer, notice: 'Customer was successfully created.' }
        format.json { render :show, status: :created, location: @customer }
      else
        format.html { render :new }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customers/1
  # PATCH/PUT /customers/1.json
  def update
    respond_to do |format|
      if @customer.update(customer_params)
        format.html { redirect_to @customer, notice: 'Customer was successfully updated.' }
        format.json { render :show, status: :ok, location: @customer }
      else
        format.html { render :edit }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customers/1
  # DELETE /customers/1.json
  def destroy
    @customer.destroy
    respond_to do |format|
      format.html { redirect_to customers_url, notice: 'Customer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def purchases
    @purchases = @customer.purchases
    @total_purchases = @purchases.reduce(0) { |a, e| a + e.price }

    respond_to do |format|
      format.html do
        flash.now[:notice] = 'Customer does not have purchases yet' if @purchases.empty?
      end
      format.json { render 'purchases/index' }
      format.pdf
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      id = params[:id] || params[:customer_id]
      @customer = Customer.find(id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customer_params
      params.require(:customer).permit(:number, :name, :detail)
    end
end
