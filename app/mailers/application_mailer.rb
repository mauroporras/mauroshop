class ApplicationMailer < ActionMailer::Base
  default from: 'reports@mauroshop.com'
  layout 'mailer'
end
