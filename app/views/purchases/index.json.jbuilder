json.array!(@purchases) do |purchase|
  json.extract! purchase, :id, :customer_id, :product_id, :branch_id, :price, :description
  json.url purchase_url(purchase, format: :json)
end
