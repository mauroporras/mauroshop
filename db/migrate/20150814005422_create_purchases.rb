class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.references :customer, index: true, foreign_key: true
      t.references :product, index: true, foreign_key: true
      t.references :branch, index: true, foreign_key: true
      t.integer :price, null: false
      t.string :description

      t.timestamps null: false
    end
  end
end
