class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.string :operation, null: false
      t.string :model_class, null: false
      t.string :model_id, null: false
      t.text :model_json, null: false

      t.timestamps null: false
    end
  end
end
