# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150814234933) do

  create_table "branches", force: :cascade do |t|
    t.string   "name",       limit: 255, null: false
    t.string   "address",    limit: 255, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "customers", force: :cascade do |t|
    t.string   "number",     limit: 255, null: false
    t.string   "name",       limit: 255, null: false
    t.string   "detail",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "customers", ["number"], name: "index_customers_on_number", unique: true, using: :btree

  create_table "logs", force: :cascade do |t|
    t.string   "operation",   limit: 255,   null: false
    t.string   "model_class", limit: 255,   null: false
    t.string   "model_id",    limit: 255,   null: false
    t.text     "model_json",  limit: 65535, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "order_details", force: :cascade do |t|
    t.integer  "order_id",    limit: 4
    t.integer  "purchase_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "order_details", ["order_id"], name: "index_order_details_on_order_id", using: :btree
  add_index "order_details", ["purchase_id"], name: "index_order_details_on_purchase_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.string   "number",     limit: 255, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "orders", ["number"], name: "index_orders_on_number", unique: true, using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "name",        limit: 255, null: false
    t.string   "description", limit: 255, null: false
    t.integer  "price",       limit: 4,   null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "purchases", force: :cascade do |t|
    t.integer  "customer_id", limit: 4
    t.integer  "product_id",  limit: 4
    t.integer  "branch_id",   limit: 4
    t.integer  "price",       limit: 4,   null: false
    t.string   "description", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "purchases", ["branch_id"], name: "index_purchases_on_branch_id", using: :btree
  add_index "purchases", ["customer_id"], name: "index_purchases_on_customer_id", using: :btree
  add_index "purchases", ["product_id"], name: "index_purchases_on_product_id", using: :btree

  create_table "settings", force: :cascade do |t|
    t.string   "var",        limit: 255,   null: false
    t.text     "value",      limit: 65535
    t.integer  "thing_id",   limit: 4
    t.string   "thing_type", limit: 30
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "settings", ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true, using: :btree

  add_foreign_key "order_details", "orders"
  add_foreign_key "order_details", "purchases"
  add_foreign_key "purchases", "branches"
  add_foreign_key "purchases", "customers"
  add_foreign_key "purchases", "products"
end
