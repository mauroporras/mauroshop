# App settings.
Setting.next_order_number = 100_000
Setting.weekly_report_email = 'mauro.porrasp@gmail.com'

# Seed the business branches.
Branch.create! [{
  name: 'Sur', address: 'Cll 1 # 6 23'
}, {
  name: 'Norte', address: 'Cll 100 # 19 31'
}, {
  name: 'Centro', address: 'Cll 12 # 4 2'
}]
# Since having a purchase without a business branch
# is like dividing by 0, this is considered the "no-branch".
Setting.void_branch_id = Branch.create!(name: '(No Branch)',
                                        address: 'N/A').id

# Seed the customers.
Customer.create! [{
  number: '123456', name: 'Arturo Lopez'
}, {
  number: '234567', name: 'Carlos Rodriguez'
}, {
  number: '345678', name: 'Daniel Acosta'
}, {
  number: '456789', name: 'Jason Martinez'
}, {
  number: '567890', name: 'Felipe Salazar'
}, {
  number: '987654', name: 'Alejandra Rodriguez'
}, {
  number: '876543', name: 'Daniela Arias'
}, {
  number: '765432', name: 'Haroll Cuervo'
}, {
  number: '654321', name: 'Jenny Perez'
}]
# Void customer for the "broken" movements.
Setting.void_customer_id = Customer.create!(number: 'N/A',
                                            name: '(Void)').id

# Seed the products.
Product.create! [{
  name: 'Producto1', price: 100_000, description: 'DProducto1'
}, {
  name: 'Producto2', price: 32_000, description: 'DProducto2'
}, {
  name: 'Producto3', price: 2_000, description: 'DProducto3'
}, {
  name: 'Producto4', price: 30_000, description: 'DProducto4'
}, {
  name: 'Producto5', price: 140_500, description: 'DProducto5'
}]
# Void product for the "broken" movements.
Setting.void_product_id = Product.create!(name: '(Void)',
                                          price: 0,
                                          description: 'N/A').id

# The "broken" movements are staying
# so you have the chance to fix them later.
# An accounting record should not be discarded just like that.
Purchase.create! [{
  id: 19,
  customer_id: 1,
  product_id: 2,
  branch_id: 1,
  price: 1000,
  created_at: '2015-08-01 15:07:30',
  updated_at: '2015-08-01 15:07:30'
}, {
  id: 20,
  customer_id: 2,
  product_id: 2,
  branch_id: 1,
  created_at: '2015-08-02 15:07:30',
  updated_at: '2015-08-02 15:07:30'
}, {
  id: 21,
  customer_id: 3,
  product_id: 1,
  branch_id: 1,
  created_at: '2015-08-02 15:07:30',
  updated_at: '2015-08-02 15:07:30'
}, {
  id: 22,
  customer_id: 4,
  product_id: 4,
  branch_id: 2,
  price: 10_000,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 23,
  customer_id: 5,
  product_id: 3,
  branch_id: 2,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 24,
  customer_id: 6,
  product_id: 5,
  branch_id: 3,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 25,
  customer_id: 7,
  product_id: 1,
  branch_id: 1,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 26,
  customer_id: 8,
  product_id: 2,
  branch_id: 1,
  price: 30_000,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 27,
  customer_id: 9,
  product_id: 5,
  branch_id: 1,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 28,
  customer_id: 2,
  product_id: 4,
  branch_id: 2,
  price: 1000,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 29,
  customer_id: 2,
  product_id: 2,
  branch_id: Setting.void_branch_id,
  description: 'dañado',
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 30,
  customer_id: 2,
  product_id: 1,
  branch_id: Setting.void_branch_id,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 31,
  customer_id: 1,
  product_id: Setting.void_product_id,
  branch_id: Setting.void_branch_id,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 32,
  customer_id: 1,
  product_id: Setting.void_product_id,
  branch_id: Setting.void_branch_id,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 33,
  customer_id: 1,
  product_id: Setting.void_product_id,
  branch_id: Setting.void_branch_id,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 34,
  customer_id: 1,
  product_id: Setting.void_product_id,
  branch_id: Setting.void_branch_id,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 35,
  customer_id: Setting.void_customer_id,
  product_id: 3,
  branch_id: Setting.void_branch_id,
  price: 9999,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 36,
  customer_id: Setting.void_customer_id,
  product_id: 1,
  branch_id: Setting.void_branch_id,
  price: 9999,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 37,
  customer_id: Setting.void_customer_id,
  product_id: 5,
  branch_id: Setting.void_branch_id,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 38,
  customer_id: 3,
  product_id: 2,
  branch_id: Setting.void_branch_id,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 39,
  customer_id: 3,
  product_id: Setting.void_product_id,
  branch_id: Setting.void_branch_id,
  price: 1000,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 40,
  customer_id: 4,
  product_id: 2,
  branch_id: Setting.void_branch_id,
  created_at: '2015-08-03 15:07:30',
  updated_at: '2015-08-03 15:07:30'
}, {
  id: 41,
  customer_id: Setting.void_customer_id,
  product_id: Setting.void_product_id,
  branch_id: Setting.void_branch_id,
  created_at: '2015-08-03 15:07:30'
}]
